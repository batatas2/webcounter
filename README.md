# Webcounter

Simple Python Webcounter with redis server

## Build
docker build -t paferr01/webcounter:1.0.0 .

## Dependencies
docker run -d  --name redis --rm redis:alpine

## Run
docker run -d --rm -p8000:5000 --name webcounter --link redis -e REDIS_URL=redis paferr01/webcounter:1.0.0

### Instal gitlab-runner
    sudo apk add gitlab-runner

### Gitlab register

gitlab-runner register -n \
--url https://gitlab.com/ \
--executor shell \
--description "develop-node" \
--tag-list "develop" \
--registration-token GR1348941rYskK2iYi8-kW9VTtZKS

### Run runner
    gitlab-runner run